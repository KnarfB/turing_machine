# accepts a 0/1 string with an odd number of 1

input: '110110101001'  
blank: '_'
start state: A
table:
  A:
    0: {write: 0, R: A}
    1: {write: 1, R: B}
    _: {write: _, L: Z}
  B:
    0: {write: 0, R: B}
    1: {write: 1, R: A}
    _: {write: _, L: Y}
  Y:  # halt - accept (yes)
  Z:  # halt - reject