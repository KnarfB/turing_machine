import sys
import PySimpleGUI as sg
from turing import *


sg.theme("DarkAmber")
sg.set_options(font=('Courier New', 12))

if len(sys.argv) > 1:
    file_name = sys.argv[1]
else:
    file_name = 'complement.tm'

transition_function = read_transition_function(file_name)

if len(sys.argv) > 2:
    initial_tape = sys.argv[2]
else:
    initial_tape = '' # empty

tm = TuringMachine(
        initial_tape,
        initial_state = "init",
        final_states = {"halt"},
        transition_function=transition_function
    )

transition_function_list = [(k1,k2)+transition_function[(k1,k2)] for (k1,k2) in transition_function]

layout = [
    [
        sg.Text(initial_tape, key='Tape',  text_color = 'tomato', relief=sg.RELIEF_GROOVE, expand_x=True, expand_y=False)
    ],
    [
        sg.Text(initial_tape, key='Head',  text_color = 'tomato', relief=sg.RELIEF_GROOVE, expand_x=True, expand_y=False)
    ],
    [
        sg.Table(transition_function_list, key='Program', headings=['curr_state', 'curr_symbol', 'next_state', 'next_symbol', 'movement'], col_widths=[10], auto_size_columns=False, num_rows=min(25, len(transition_function)),  display_row_numbers=False, justification='center', select_mode=sg.TABLE_SELECT_MODE_NONE, expand_x=True, expand_y=True),
    ]
]
 
window = sg.Window('Turing Machine - '+file_name, layout, finalize=True, resizable=True, return_keyboard_events=True)

while True:
    window['Tape'].update(tm.get_tape())
    head = '^'.rjust(1+tm.get_head_position())
    window['Head'].update(head)
    window['Program'].update()

    event, values = window.read()
    print(event)
    if event==sg.WIN_CLOSED:
        break
    if event==' ' or event=='space:65': # SPACE bar pressed, 'space:65' seen on debian sid bspwm
        try:
            tm.step()
            if tm.final():
                sg.popup_auto_close('Turing Machine halted')
                
        except Exception as e:
            sg.popup_error_with_traceback(f'Exception occured', e)
            break

window.close()