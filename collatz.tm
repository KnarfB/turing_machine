# Collatz Conjecture https://en.wikipedia.org/wiki/Collatz_conjecture 
# halts when 1 is reached 
blank: _
input: '10000001' # 129
start state: AA
table:
  AA: {0: {write: _, R: AA}, 1: {write: 1, R: A1}, _: {write: _, L: HH}} # initial state, checking for single 1
  A1: {0: {write: 0, R: RR}, 1: {write: 1, R: RR}, _: {write: _, L: HH}} # halt on single 1, go right else
  RR: {0: {write: 0, R: RR}, 1: {write: 1, R: RR}, _: {write: _, L: KK}} # go to the right end
  KK: {0: {write: _, L: KK}, 1: {write: 0, L: S2}, _: {write: _, R: HH}} # div 2: kill trailing (lsb) 0, halt on all 0
  C0: {0: {write: 0, L: C0}, 1: {write: 1, L: C1}, _: {write: _, R: RR}} # *3 with carry in 0
  C1: {0: {write: 1, L: C0}, 1: {write: 0, L: C2}, _: {write: 1, L: C0}} # *3 with carry in 1
  C2: {0: {write: 0, L: C1}, 1: {write: 1, L: C2}, _: {write: 0, L: C1}} # *3 with carry in 2
  S2: {0: {write: 0, L: C1}, 1: {write: 1, L: C2}, _: {write: _, R: HH}} # variant of C1, breaks 1-4-2 cycle
  HH: # halt
