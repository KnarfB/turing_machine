# computes bitwise complement of a binary number

input: '1011'
blank: '_'
start state: A
table:
  A:
    0: {write: 1, R: A}
    1: {write: 0, R: A}
    _: {write: _, L: B}
  B:
    0: {write: 0, L: B}
    1: {write: 1, L: B}
    _: {write: _, R: Z}    
  Z:
