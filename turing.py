# turing machine interpreter.
# turing table/programm syntax matches a subset of interactive online simulator https://turingmachine.io/
# no shortcuts are allowed, each state transition must be fully specified (if present), see exampe .tm files 

import sys
import yaml

class TuringMachine:
    def __init__(self, blank=' ', input=[], state='A', table=None, head=0):
        self.blank = blank
        self.tape = [blank] if len(input)==0 else input
        self.state = state
        self.table = table
        self.head = head # self.tape[self.head] must always be valid

    def get_symbol(self):
        symbol = self.tape[self.head]
        try: # return 0 instead of '0' etc.
            return int(symbol)
        except ValueError:
            pass # not a digit, return a char
        return symbol

    def get_tape(self, fancy=True):
        symbols = [str(symbol) for symbol in self.tape]
        tape_string = ''.join(symbols)
        if fancy:
            tape_string = tape_string[:self.head] + f'{self.get_state()}>' + tape_string[self.head:]
            blank = str(self.blank) if isinstance(self.blank,int) else self.blank
            tape_string = tape_string.strip(blank)
        return tape_string 

    def get_head(self):
        return self.head

    def get_state(self):
        return self.state

    def step(self):
        if self.halted():
            return False
        symbol = self.get_symbol()
        state = self.get_state()
        next = self.table[state][symbol]
        if 'write' in next: # if not specified, don't overwrite symbol
            self.tape[self.head] = next['write']
        if 'L' in next:
            if self.head==0:
                self.tape = [self.blank] + self.tape
            else:
                self.head -= 1
            try:
                if next['L'] is not None:
                    self.state = next['L']
            except TypeError:
                 pass # next state not specified, do not change state
        elif 'R' in next:
            self.head += 1
            if self.head==len(self.tape):
                self.tape = self.tape + [self.blank]
            try:
                if next['R'] is not None:
                    self.state = next['R']
            except TypeError:
                pass # next state not specified, do not change state
        return True

    def halted(self):
        state = self.get_state()
        if self.table is None or state not in self.table:
            return True
        symbol = self.get_symbol()
        if self.table[state] is None or symbol not in self.table[state]:
            return True
        return False

### main ###
if len(sys.argv) > 1:
    file_name = sys.argv[1]
else:
    print('.tm file argument missing, exit.')
    sys.exit(-1)

with open(file_name, "r") as stream:
    try:
        tm = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc) # seen in binary_increment1.tm
        sys.exit(-2)

blank = tm['blank']
state = tm['start state']
table = tm['table']
head = 0 # on the leftmost input symbol if any or on a blank else

if len(sys.argv) > 2:
    tape = sys.argv[2]
else:
    tape = input()
tape = [ c for c in tape]

machine = TuringMachine(blank, tape, state, table, head)

steps = 0
while not machine.halted():
    print(machine.get_tape())
    machine.step()
    steps += 1
print(f'{machine.get_tape()}    HALT after {steps} steps')
