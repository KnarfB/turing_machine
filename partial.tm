# will this TM stop for any 0/1 input string?

input: ''
blank: '_'
start state: A
table:
  A:
    0: {write: 0, R: A}
    1: {write: 1, R: A1}
    _: {write: _, L: B}

  A1:
    0: {write: 1, R: A}
    1: {write: 0, R: A2}
    _: {write: _, L: B}

  A2:
    0: {write: 0, R: A1}
    1: {write: 1, R: A3}
    _: {write: _, L: B}

  A3:
    0: {write: 1, L: A2}
    1: {write: 0, L: A2}
    _: {write: _, L: B}

  B:
