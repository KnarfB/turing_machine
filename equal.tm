# input: a string consisting of a's and b's in any mix
# machine halts in state Y if the number of a's and number of b's are equal 
# machine halts in state N else

input: 'aab'
blank: '_'
start state: A
table:

  A: # going right
    a: {write: x, R: Aa}
    b: {write: x, R: Ab}
    x: {write: x, R: A}
    _: {write: _, L: Y}

  Aa: # saw a, continue right
    a: {write: a, R: Aa}
    b: {write: x, L: Aab}
    x: {write: x, R: Aa}
    _: {write: _, L: N}

  Ab: # saw b, continue right
    a: {write: x, L: Aab}
    b: {write: b, R: Ab}
    x: {write: x, R: Ab}
    _: {write: _, L: N}

  Aab: # saw a and b, return left
    a: {write: a, L: Aab}
    b: {write: b, L: Aab}
    x: {write: x, L: Aab}
    _: {write: _, R: A} # restart

  Y: # yes equal
  
  N: # not equal

  # task (trivial): extend the TM such that is stops in one of 3 states: A (more a's), B (more b's) or Z (equal) 