# original from https://turingmachine.io/
# raises exception: 
# while constructing a mapping
# found unhashable key
#  in "binary_increment1.tm", line 10, column 5

# Adds 1 to a binary number.
input: '1011'
blank: ' '
start state: right
table:
  # scan to the rightmost digit
  right:
    [1,0]: R
    ' '  : {L: carry}
  # then carry the 1
  carry:
    1      : {write: 0, L}
    [0,' ']: {write: 1, L: done}
  done:


# Exercises:

# • Modify the machine to always halt on the leftmost digit
#   (regardless of the number's length).
#   Hint: add a state between carry and done.

# • Make a machine that adds 2 instead of 1.
#   Hint: 2 is '10' in binary, so the last digit is unaffected.
#   Alternative hint: chain together two copies of the machine from
#   the first exercise (renaming the states of the second copy).

# • Make a machine to subtract 1.
#   To simplify things, assume the input is always greater than 0.
