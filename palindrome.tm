# from https://turingmachine.io/
# slight mods to work with turing.py

# Accepts palindromes made of the symbols 'a' and 'b'
input: 'abba' # try a, ab, bb, babab
blank: ' '
start state: start
# A palindrome is either the empty string, a single symbol,
# or a (shorter) palindrome with the same symbol added to both ends.
table:
  start:
    a: {write: ' ', R: haveA}
    b: {write: ' ', R: haveB}
    ' ': {R: accept} # empty string
  haveA:
    a: R
    b: R
    ' ': {L: matchA}
  haveB:
    a: R
    b: R
    ' ': {L: matchB}
  matchA:
    a: {write: ' ', L: back} # same symbol at both ends
    b: {R: reject}
    ' ': {R: accept} # single symbol
  matchB:
    a: {R: reject}
    b: {write: ' ', L: back} # same symbol at both ends
    ' ': {R: accept} # single symbol
  back:
    a: L
    b: L
    ' ': {R: start}
  accept:
  reject:


# Exercise:

# • Modify the machine to include 'c' in the symbol alphabet,
#   so it also works for strings like 'cabbac'.
