# busy beaver with 2 states and 2 symbols
# see https://webusers.imj-prg.fr/~pascal.michel/ha.html#tm22

input: ''
blank: '_'
start state: A
table:
  A:
    1: {write: 1, L: B}
    _: {write: 1, R: B}
  B:
    1: {write: 1, R: Z}
    _: {write: 1, L: A}    
  Z:
