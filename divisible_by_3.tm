# from https://turingmachine.io/

input: '1011'
blank: '_'
start state: A
table:
  A:
    0: {write: 0, R: A}
    1: {write: 1, R: A}
    _: {write: _, L: B}
  B:
    0: {write: 1, L: C}
    1: {write: 0, L: B}
    _: {write: 1, L: Z}
  C:
    0: {write: 0, L: C}
    1: {write: 1, L: C}
    _: {write: _, R: Z}
  Z: